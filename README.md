This repository aims to provide a browsable backup copy of [Scilab moinmoin wiki](https://wiki.scilab.org).

## Offline building

To ease preview, the wiki content is copied in this repo.

## GitLab CI

Gitlab pages expects to put all your HTML files in the `public/` directory.

## GitLab Wiki

Converted markdown files are pushed to the project wiki to ease copy and re-usage. 