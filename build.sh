#!/usr/bin/env bash

set -e

# first stage, from raw html to markdown
cp -alf wiki/* markdown/
find markdown -iname "*.html" -type f -print -exec sh -c 'pandoc "${0}" -f html-native_divs-native_spans -t markdown+grid_tables --filter pandoc_filter.py -o "${0%.html}.md"' {} \; -delete
# update link to internal page md
find markdown -name '*.md' -printf '%f\n' | \
    sed -e 'p' \
        -e 's/\.md$//g' \
        -e 's#(attachments#(/attachments#g' \
        -e 's#\([0-9]\+\)(2f)\([0-9]\+\)(2f)\([0-9]\+\)#\1%2F\2%2F\3#g' \
        -e 's/C99(2f)C(2f)Z(2f2e2e2e29)/C99%2FC%2FZ%2F.../g' \
        -e 's/import/іmport/g' \
        -e 's/2008\/2010/2008-2010/g' \
        -e 's/(20)/-/g' \
        -e 's/(2022)/-ᐦ/g' \
        -e 's/(202620)/-and-/g' \
        -e 's/(202727)/-ᐦ/g' \
        -e 's/(2028)/-(/g' \
        -e 's/(202b20)/-+-/g' \
        -e 's/(202d20)/-/g' \
        -e 's/(202d202e)/-./g' \
        -e 's/(202e)/-./g' \
        -e 's/(203a20)/∶-/g' \
        -e 's/(203c)/ᐸ/g' \
        -e 's/(203c2d3e20)/-⇔-/g' \
        -e 's/(203e3d)/>=/g' \
        -e 's/(203f)/❓/g' \
        -e 's/(205b)/-[/g' \
        -e 's/(20c3a020)/-à-/g' \
        -e 's/(2220)/"-/g' \
        -e 's/(27)/’/g' \
        -e 's/(272720)/"-/g' \
        -e 's/(27c389)/’É/g' \
        -e 's/(2829)/()/g' \
        -e 's/(282920)/()-/g' \
        -e 's/(29)/)/g' \
        -e 's/(2920)/)-/g' \
        -e 's/(2b)/+/g' \
        -e 's/(2b2b20)/++-/g' \
        -e 's/(2b2b203c2d3e20)/++-to-/g' \
        -e 's/(2c)/,/g' \
        -e 's/(2c20)/,/g' \
        -e 's/(2d)/-/g' \
        -e 's/(2e)/./g' \
        -e 's/(2e20)/.-/g' \
        -e 's/(2f)/\//g' \
        -e 's/(2f5b)/\/[/g' \
        -e 's/(3a20)/∶-/g' \
        -e 's/(3a29)//g' \
        -e 's/(3d)/=/g' \
        -e 's/(3f)/❓/g' \
        -e 's/(40)/@/g' \
        -e 's/(5b)/[/g' \
        -e 's/(5d)/]/g' \
        -e 's/(5d20)/]/g' \
        -e 's/(c2b0)/°/g' \
        -e 's/(c380)/À/g' \
        -e 's/(c389)/é/g' \
        -e 's/(c3a2)/â/g' \
        -e 's/(c3a8)/è/g' \
        -e 's/(c3a9)/é/g' \
        -e 's/(c3a920)/é-/g' \
        -e 's/(c3a92920)/é)-/g' \
        -e 's/(c3a92d)/é-/g' \
        -e 's/(c3a92f)/é∶-/g' \
        -e 's/(c3ae)/î/g' \
        -e 's/(c3b4)/ô/g' >all_md_files;

cd markdown
# relink references to wiki.scilab.org
sed -i "s#https\?:\/\/wiki.scilab.org#https://scilab.gitlab.io/legacy_wiki#g" ./*.md
sed -i "s#https\?:\/\/wiki.project.scilab.org#https://scilab.gitlab.io/legacy_wiki_project#g" ./*.md
# update links
while read -r name_with_ext; do
    read -r fullname;
    name="$(basename "$name_with_ext" .md)"
    echo "$name     $fullname"
    sed -i "s#\.\/$name.html#$fullname#g" ./*.md
done <../all_md_files
sed -i "s#(attachments/#(/attachments/#g" ./*.md
# move to correct location
while read -r name_with_ext; do
    read -r fullname;
    if [ -f "$name_with_ext" ] && [ "$name_with_ext" != "$fullname".md ]; then
        mkdir --parents "$(dirname "$fullname")"
        mv "$name_with_ext" "$fullname".md
    fi
done <../all_md_files
# set default page to home.md
cp -a Contents.md home.md
