// Copyright (C) - 2012 - Pierre Chevalier
// Reference
// http://www.beebac.com/publication/53365/comment-resoudre-le-rubiks-cube-avec-un-code-informatique
// This file must be used under the terms of the 
// Creative Commons : Attribution
// http://creativecommons.org/licenses/by/3.0/fr/

// 1. Afficher un cube en 3d.
// La liste des sommets contient les doordonnées dans l'espace 
// des huit sommets définissant mon cube.
sommets =[-0.5 -0.5 -0.5;
-0.5 -0.5  0.5;
-0.5  0.5 -0.5;
-0.5  0.5  0.5;
0.5 -0.5 -0.5;
0.5 -0.5  0.5;
0.5  0.5 -0.5;
0.5  0.5  0.5];
// Le cube est composé de six faces qui relient les huit 
// différents sommets comme suis.
cube = [2 4 8 6;  //face en haut
7 5 1 3;  //face en bas
5 6 8 7;  //face gauche
3 4 8 7;  //face avant
1 2 4 3;  //face droite
1 2 6 5  //face arrière
];
// La liste des couleurs dans le même ordre que les facettes.
tcolor = [color("white") color("yellow") color("blue") color("orange") color("green") color("red")]';
// Conversion de l'écriture précédente en des termes que saura interpréter la fonction plot3d.
x = matrix(sommets(cube,1),size(cube,1),length(sommets(cube,1))/size(cube,1))' ;
y = matrix(sommets(cube,2),size(cube,1),length(sommets(cube,1))/size(cube,1))' ;
z = matrix(sommets(cube,3),size(cube,1),length(sommets(cube,1))/size(cube,1))' ;
clf();
// On évite d'afficher une couleur derrière les faces qui empêche de voir le dos du cube.
xset("hidden3d",0);
// On utilise la fonction plot3d pour afficher le cube.
plot3d(x,y,list(z,tcolor));
// Avec un titre, ça fait toujours plus propre.
xtitle('Un cube simple en 3d');

//////////////////////////////////////////////////////////
//
// 2.
//

// Cette instruction marque le début d'une fonction qui accepte 
// les arguments : position (une liste de trois 
// nombre réels) et couleurs (une liste de six code attribués à six couleurs différentes)
function plotcube(position, couleurs)
    // Afficher un cube en 3d
    // La liste des positions des sommets (on utilise l'argument 
    // d'appel : position pour décaler tous les sommets du 
    // cube dans une même direction, donc pour décaler le cube dans cette direction).
    sommets =[
    -0.5+position(1) -0.5+position(2) -0.5+position(3);
    -0.5+position(1) -0.5+position(2)  0.5+position(3);
    -0.5+position(1)  0.5+position(2) -0.5+position(3);
    -0.5+position(1)  0.5+position(2)  0.5+position(3);
    0.5+position(1) -0.5+position(2) -0.5+position(3);
    0.5+position(1) -0.5+position(2)  0.5+position(3);
    0.5+position(1)  0.5+position(2) -0.5+position(3);
    0.5+position(1)  0.5+position(2)  0.5+position(3)
    ];
    // Le cube est composé de six faces
    cube = [2 4 8 6;  //face du haut
    7 5 1 3;  //dace du bas
    5 6 8 7;  //face gauche
    3 4 8 7;  //face avant
    1 2 4 3;  //face droite
    1 2 6 5  //face arrière
    ];
    // La liste des couleurs dans le même ordre que les facettes.
    tcolor = couleurs' ;
    // Conversion pour fonction plot3d
    x = matrix(sommets(cube,1),size(cube,1),length(sommets(cube,1))/size(cube,1))' ;
    y = matrix(sommets(cube,2),size(cube,1),length(sommets(cube,1))/size(cube,1))' ;
    z = matrix(sommets(cube,3),size(cube,1),length(sommets(cube,1))/size(cube,1))' ;
    //clf();
    //On évite d'afficher une couleur derrière les faces 
    // qui empêche de voir le dos du cube.
    xset("hidden3d",0);
    plot3d(x,y,list(z,tcolor));
endfunction

function AfficheRubix(ObjetRubix)
    clf();
    for i=1:27
        plotcube(ObjetRubix(i,1:3),ObjetRubix(i,4:9));
    end;
endfunction

function [ObjetRubix]=CreationRubix()
    // On commence à déclarer ObjetRubix comme une liste vide.
    ObjetRubix=[];
    // Pour i prenant les valeurs de -1, 0 et 1,
    for x=-1:1
        // Pour j prenant les valeurs de -1, 0 et 1,
        for y=-1:1
            // Pour k prenant les valeurs de -1, 0 et 1,
            for z=-1:1
                // On rajoute à chaque fois une ligne dans la matrice qui 
                // correspond au cube de coordonnées : [x y z]
                // Pour commencer, on oriente les couleurs de la 
                // même façon pour tous les cubes.
                ObjetRubix=[ObjetRubix;x,y,z,color("white"),color("yellow"),..
                color("blue"),color("orange"),color("green"),color("red")];
            end;
        end;
    end;
    //L'objet de cette nouvelle boucle est d'enlever les cours que l'on 
    // ne voit pas (à l'intérieur du Rubik's cube) 
    for i=1:27// Pour chaque sous-cube,
        if ObjetRubix(i,1)==-1 then 
            // si x=1 (on notre cube est sur la face de droite)
            //Alors on supprime la couleur du côté gauche du sous-cube i
            ObjetRubix(i,3+3)=0 
        end;
        if ObjetRubix(i,1)==1 then 
            // si face gauche
            ObjetRubix(i,3+5)=0 // On supprime la couleur du côté droit
        end;
        if ObjetRubix(i,1)==0 then 
            // si la tranche est au milieu par rapport à l'axe des x
            //On supprime la couleur du côté gauche et du côté droit
            ObjetRubix(i,3+3)=0 
            ObjetRubix(i,3+5)=0 
        end;
        if ObjetRubix(i,2)==-1 then 
            // si face arrière
            // On supprime la couleur de l'avant du sous-cube
            ObjetRubix(i,3+4)=0 
        end;
        if ObjetRubix(i,2)==1 then 
            // si face avant
            // On supprime la couleur de l'arrière du sous-cube
            ObjetRubix(i,3+6)=0 
        end;
        if ObjetRubix(i,2)==0 then 
            // si tranche du milieu par rapport à l'axe des y
            // On supprime la couleur de l'avant et de l'arrière du sous-cube
            ObjetRubix(i,3+4)=0 
            ObjetRubix(i,3+6)=0
        end;
        if ObjetRubix(i,3)==-1 then 
            // si face du bas
            // on suprimme la couleur du haut du sous cube
            ObjetRubix(i,3+1)=0 
        end;
        if ObjetRubix(i,3)==1 then 
            // si face du haut
            // on suprimme la couleur du bas du sous cube
            ObjetRubix(i,3+2)=0 
        end;
        if ObjetRubix(i,3)==0 then 
            // si face coincee entre bas et haut
            // on suprimme la couleur du bas et du haut du sous cube
            ObjetRubix(i,3+1)=0 
            ObjetRubix(i,3+2)=0
        end; 
    end;
endfunction 

[ObjetRubix]=CreationRubix();
AfficheRubix(ObjetRubix)
