// Copyright (C) 2013 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
function scattermatrixXY(varargin)
    // Plot an X vx Y scatter plot matrix
    //
    // Calling Sequence
    //   scattermatrixXY(x,y)
    //   scattermatrixXY(x,y,key1,value1,key2,value2,...)
    //   scattermatrixXY(x,y,"xlabels",xlabels)
    //   scattermatrixXY(x,y,"ylabels",ylabels)
    //   scattermatrixXY(x,y,"ptsize",ptsize)
    //   scattermatrixXY(x,y,"valuelabels",valuelabels)
    //   scattermatrixXY(x,y,"symbol",symbol)
    //
    // Parameters
    //   x : a n-by-ninput matrix of doubles, the x datas
    //   y : a n-by-noutput matrix of doubles, the y datas
    //   xlabels : a ninput-by-1 matrix of strings, the x labels (default="")
    //   ylabels : a noutput-by-1 matrix of strings, the y labels (default="")
    //   ptsize : a 1-by-1 matrix of doubles, integer value, positive, the number pixels for the dots (default ptsize=2)
    //   valuelabels : a 1-by-1 matrix of booleans, set to true to print the x and y value labels (default=%t)
    //   symbol : a 1-by-1 matrix of strings, the point symbols (default="b.")
    //
    // Description
    // Plots a matrix of scatter plots representing the 
    // dependencies of Y vs X.
    // 
    // Authors
    // Copyright (C) 2013 - Michael Baudin

    [lhs,rhs]=argn()
    apifun_checkrhs ( "scattermatrixXY" , rhs , 2:12 )
    apifun_checklhs ( "scattermatrixXY" , lhs , 0:1 )
    //
    x = varargin ( 1 )
    y = varargin ( 2 )
    //
    // 1. Set the defaults
    default.xlabels = []
    default.ylabels = []
    default.ptsize = 2
    default.valuelabels = %t
    default.symbol = "b."
    //
    // 2. Manage (key,value) pairs
    options=apifun_keyvaluepairs (default,varargin(3:$))
    //
    // 3. Get parameters
    xlabels=options.xlabels
    ylabels=options.ylabels
    ptsize = options.ptsize
    valuelabels = options.valuelabels
    symbol = options.symbol
    //
    // Check Type
    apifun_checktype ( "scattermatrixXY" , x , "x" , 1 , "constant" )
    apifun_checktype ( "scattermatrixXY" , y , "y" , 2 , "constant" )
    if (xlabels<>[]) then
        apifun_checktype ( "scattermatrixXY" , xlabels , "xlabels" , 3 , "string" )
    end
    if (ylabels<>[]) then
        apifun_checktype ( "scattermatrixXY" , ylabels , "ylabels" , 3 , "string" )
    end
    apifun_checktype ( "scattermatrixXY" , ptsize , "ptsize" , 3 , "constant" )
    apifun_checktype ( "scattermatrixXY" , valuelabels , "valuelabels" , 3 , "boolean" )
    apifun_checktype ( "scattermatrixXY" , symbol , "symbol" , 3 , "string" )
    //
    // Check Size
    n=size(x,"r");
    ninput=size(x,"c")
    noutput=size(y,"c")
    //
    apifun_checkdims ( "scattermatrixXY" , x , "x" , 1 , [n ninput] )
    apifun_checkdims ( "scattermatrixXY" , y , "y" , 2 , [n noutput] )
    if (xlabels<>[]) then
        apifun_checkvector ( "scattermatrixXY" , xlabels , "xlabels" , 3 , ninput )
    end
    if (ylabels<>[]) then
        apifun_checkvector ( "scattermatrixXY" , ylabels , "ylabels" , 3 , noutput )
    end
    apifun_checkscalar ( "scattermatrixXY" , ptsize , "ptsize" , 3 )
    apifun_checkscalar ( "scattermatrixXY" , valuelabels , "valuelabels" , 3 )
    apifun_checkscalar ( "scattermatrixXY" , symbol , "symbol" , 3 )
    //
    // Check Content
    apifun_checkgreq ( "scattermatrixXY" , ptsize , "ptsize" , 3 , 1 )
    apifun_checkflint ( "scattermatrixXY" , ptsize , "ptsize" , 3 )
    //
    iserr=%f
    if (xlabels<>[]&ylabels==[]) then
        iserr=%t
    end
    if (xlabels==[]&ylabels<>[]) then
        iserr=%t
    end
    if (iserr) then
        errmsg=gettext("%s: xlabels and ylabels are both expected.")
        error(msprintf(errmsg,"scattermatrixXY"))
    end
    //
    scf();
    for j=1:noutput
        for i=1:ninput
            p=(j-1)*ninput + i;
            subplot(ninput,noutput,p);
            plot(x(:,i),y(:,j),symbol);
            if (xlabels<>[]&ylabels<>[]) then
                xtitle("",xlabels(i),ylabels(j))
            else
                xtitle("","","")
            end
            e=gce();
            if (~valuelabels) then
                e.auto_ticks(1:3)="off";
            end
            e.children.children.mark_size=ptsize;
        end
    end
endfunction

if (%f) then
    m=1000;
    x1=grand(m,1,"def");
    x2=grand(m,1,"def");
    x3=grand(m,1,"def");
    y1=2*x1.*x2+x3;
    y2=-3*x1+x2.^2-2*x3;
    y3=sin(x1)-3*x2+3*x3;
    x=[x1,x2,x3];
    y=[y1,y2,y3];
    //
    xlabels=["X1","X2","X3"];
    ylabels=["Y1","Y2","Y3"];
    // No labels
    scattermatrixXY(x,y);
    // With labels
    scattermatrixXY(x,y,"xlabels",xlabels,"ylabels",ylabels);
    // Without XY value labels
    scattermatrixXY(x,y,"valuelabels",%f);
    // Without XY value labels, and XY labels
    scattermatrixXY(x,y,"valuelabels",%f,..
    "xlabels",xlabels,"ylabels",ylabels);
    // Set the point size
    scattermatrixXY(x,y,"ptsize",1);
    // With red crosses
    scattermatrixXY(x,y,"symbol","rx");
    // 
    // Another dataset
    m=1000;
    x1=grand(m,1,"nor",0,1);
    x2=grand(m,1,"def")*2-1;
    y1=x1.^2+x2;
    y2=-3*x1+x2.^2;
    y3=x1-3*exp(x2);
    x=[x1,x2];
    y=[y1,y2,y3];
    //
    xlabels=["X1","X2"];
    ylabels=["Y1","Y2","Y3"];
    // No labels
    scattermatrixXY(x,y);
    // With labels, and red circles
    scattermatrixXY(x,y,"xlabels",xlabels,"ylabels",ylabels,..
    "symbol","ro");
end
