#include "megaFunc.h"

/**
 * scilab version
 */
#define VERSION 5.0

/**
 * The main procedure. It can do the following:
 *  - do nothing
 *  - sleep
 *
 * @code
 * for ( i = 0 ; i < 5 ; i++ ) { megaFunc(i) ; }
 * @endcode
 * Which compute \f$(x_1,y_1)\f$ sometimes.
 * @param argc the command line
 * @param argv the number of options in the command line.
 * @return whatever
 * @author jb silvy
 */
int main( char ** argc, int argv )
{
  return megaFunc( 3 ) ;

}
