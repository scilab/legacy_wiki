#include "megaFunc.h"

int megaFunc( int x  )
{
  point3d point ;
  point3d.x = x ;
  point3d.y = x + 1 ;
  point3d.z = x + 2 ;

  return (int)floor( point3d.x ) ;
}
