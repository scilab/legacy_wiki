/**
 * @addtogroup structure_enum_etc
 */
/** @{ */

/**
 * A structure to represent 3d vectors
 */
typedef struct
{
  /*@{*/
  double x ; /**< the x coordinate */
  double y ; /**< the y coordinate */
  double z ; /**< the z coordinate */
  /*@}*/
  /**
    * @name group 2
    */
  /*@{*/
  char * name ; /**< the name of the point */
  int    namelength ; /**< the size of the point name */
  /*@}*/
} point3d ;

/**
 * Enumeration of space dimension
 */
typedef enum 
{ 
  UND,    /**< 1D */
  DEUXD,  /**< 2D */
  TROISD  /**< 3D */
} dimensions ;

/** @} */

/**
 * @addtogroup functions
 */
/** @{ */

/**
 * The point3d structure is stupid
 * @param[in] x the modified input
 * @return \f$x + 1\f$
 */
int megaFunc( int * x ) ;

/** @} */
